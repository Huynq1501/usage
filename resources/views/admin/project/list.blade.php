@extends('adminlte::page')
@section('content')
    <body>
    <div class="container mt-5">
        <h2 class="mb-4">List Project</h2>
        <div class="col-md-12 text-right mb-5">
            <a class="btn btn-success" href="javascript:void(0)" id="create-project"> Create New Project</a>
        </div>
        <table class="table table-bordered project-table">
            <thead>
            <tr>
                <th>No</th>
                <th>Project Name</th>
                <th>Detail</th>
                <th>Duration from</th>
                <th>Duration to</th>
                <th>Revenue</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <!-- Show form add/edit Project  -->
    <div class="modal fade" id="projectModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="projectForm" name="projectForm" class="form-horizontal">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="project_name" name="project_name"
                                       placeholder="Enter project name" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Detail</label>
                            <div class="col-sm-12">
                                <textarea type="text" class="form-control" id="detail" name="detail"
                                          placeholder="Enter detail project..." rows="2" required=""></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Duration from</label>
                            <div class="col-sm-12">
                                <input type="date" id="duration_from" name="duration_from"
                                       class="form-control" placeholder="Birthday">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Duration to</label>
                            <div class="col-sm-12">
                                <input type="date" id="duration_to" name="duration_to"
                                       class="form-control" placeholder="Birthday">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Revenue</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" id="revenue" name="revenue"
                                       placeholder="Enter revenue($USD)" required="">
                            </div>
                        </div>
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveProject" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End show form add/edit project -->
    </body>
@endsection
@section('js')
    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('.project-table').DataTable({
                "bDestroy": true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('projects.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'project_name', name: 'project_name'},
                    {data: 'detail', name: 'detail'},
                    {data: 'duration_from', name: 'duration_from'},
                    {data: 'duration_to', name: 'duration_to'},
                    {data: 'revenue', name: 'revenue'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });
        });

        $('#create-project').click(function () {
            $('#saveProject').val("create-project");
            $('#id').val('');
            $('#projectForm').trigger("reset");
            $('#modelHeading').html("Create New Project");
            $('#projectModel').modal('show');
        });

        $('body').on('click', '.editProject', function () {
            var id = $(this).data('id');
            $.get("{{ route('projects.index') }}" + '/' + id + '/edit', function (data) {
                $('#modelHeading').html("Edit Project");
                $('#saveProject').val("edit-user");
                $('#projectModel').modal('show');
                $('#id').val(data.id);
                $('#project_name').val(data.project_name);
                $('#detail').val(data.detail);
                $('#duration_from').val(data.duration_from);
                $('#duration_to').val(data.duration_to);
                $('#revenue').val(data.revenue);
            })
        });

        $('#saveProject').click(function (e) {
            e.preventDefault();
            $(this).html('Sending..');

            $.ajax({
                data: $('#projectForm').serialize(),
                url: "{{ route('projects.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#projectForm').trigger("reset");
                    $('#projectModel').modal('hide');
                    location.reload();
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveProject').html('Save Changes');
                }
            });
        });
        $('body').on('click', '.deleteProject', function () {
            const id = $(this).data("id");
            const result = confirm("Are You sure want to delete project !");
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ route('projects.store') }}" + '/' + id,
                    success: function (data) {
                        location.reload();
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else {
                return false;
            }
        });

    </script>


@endsection
