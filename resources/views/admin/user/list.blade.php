@extends('adminlte::page')
@section('plugins.Select2', true)
@section('content')


    <body>
    <div class="container mt-5">
        <h2 class="mb-4">List User</h2>
        <div class="col-md-12 text-right mb-5">
            <a class="btn btn-success" href="javascript:void(0)" id="createNewUser"> Create New User</a>
        </div>
        <table class="table table-bordered yajra-datatable">
            <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>Birthday</th>
                <th>Phone</th>
                <th>Address</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="ajaxModel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modelHeading"></h4>
                </div>
                <div class="modal-body">
                    <form id="userForm" name="userForm" class="form-horizontal">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"
                                       required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-12">
                                <input type="email" class="form-control" id="email" name="email"
                                       placeholder="Enter email" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="password" name="password"
                                       placeholder="Enter password" value="" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password_confirmation" class="col-sm-2 control-label">Retype Password</label>
                            <div class="col-sm-12">
                                <input type="password" class="form-control" id="password_confirmation"
                                       name="password_confirmation"
                                       placeholder="Enter password" value="" required="">


                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <x-adminlte-select2 id="gender" name="gender" label="Gender"
                                                    label-class="text-lightblue"
                                                    igroup-size="lg" data-placeholder="Select an option..." eee>
                                    <x-slot name="prependSlot">
                                        <div class="input-group-text bg-gradient-info">
                                            <i class="fas fa-venus-mars "></i>
                                        </div>
                                    </x-slot>
                                    <option/>
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Unknown</option>
                                </x-adminlte-select2>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Birthday</label>
                            <div class="col-sm-12">
                                <input type="date" class="form-control" id="birthday" name="birthday"
                                       placeholder="Birthday" value="" required="">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-12">
                                <input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter phone"
                                       value="" required="">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" id="address" name="address"
                                       placeholder="Enter address" value="" require="">
                            </div>
                        </div>

                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>

@endsection
@section('js')

    <script type="text/javascript">
        $(function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = $('.yajra-datatable').DataTable({
                "bDestroy": true,
                processing: true,
                serverSide: true,
                ajax: "{{ route('users.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'gender', name: 'gender'},
                    {data: 'birthday', name: 'birthday'},
                    {data: 'phone', name: 'phone'},
                    {data: 'address', name: 'address'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
        $('#createNewUser').click(function () {
            $('#saveBtn').val("create-user");
            $('#id').val('');
            $('#userForm').trigger("reset");
            $('#modelHeading').html("Create New User");
            $('#ajaxModel').modal('show');
        });

        $('body').on('click', '.editUser', function () {
            var id = $(this).data('id');
            $.get("{{ route('users.index') }}" + '/' + id + '/edit', function (data) {
                $('#modelHeading').html("Edit User");
                $('#saveBtn').val("edit-user");
                $('#ajaxModel').modal('show');
                $('#id').val(data.id);
                $('#name').val(data.name);
                $('#email').val(data.email);
                $('#password').val(data.password);
                $('#password_confirmation').val(data.password);
                $('#gender').val(data.gender);
                $('#birthday').val(data.birthday);
                $('#phone').val(data.phone);
                $('#address').val(data.address);
            })
        });

        $('#saveBtn').click(function (e) {
            e.preventDefault();
            $(this).html('Sending..');

            $.ajax({
                data: $('#userForm').serialize(),
                url: "{{ route('users.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    $('#userForm').trigger("reset");
                    $('#ajaxModel').modal('hide');
                    location.reload();
                    table.draw();
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#saveBtn').html('Save Changes');
                }
            });
        });
        $('body').on('click', '.deleteUser', function () {
            var id = $(this).data("id");
            var result = confirm("Are You sure want to delete !");
            if (result) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ route('users.store') }}" + '/' + id,
                    success: function (data) {
                        location.reload();
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            } else {
                return false;
            }
        });
    </script>
    <script>
        $().ready(function () {
            $('#userForm').validate({
                rules: {
                    name: {
                        required: true
                    },
                    email: {
                        required: true,
                        BSEmail: true
                    },
                    password: {
                        required: true,
                        minlength: 6
                    },
                    password_confirmation: {
                        required: true,
                        minlength: 6,
                        equalTo: "#password"
                    },
                    gender: "required",
                    birthday: {
                        required: true,
                        date: true,
                        dateLessThan: '#expiredDate'
                    }

                },
                messages: {
                    name: "Please enter your name",
                    email: {
                        required: "Please enter your email",
                        BSEmail: "Only Beet Soft.com email address are allowed."
                    },
                    password: {
                        required: "Please enter your password",
                        minlength: "Your password must be at least 6 characters long"
                    },
                    password_confirmation: {
                        required: "Please confirm password again",
                        minlength: "Your password must be at least 6 characters long",
                        equalTo: "Password & confirm password not match"

                    },
                    gender: "Select your gender",
                    birthday: {
                        required: "Please enter your date of birth",
                        date: "Your date of birth must be a date",
                        dateLessThan: "your birthday must be before today",
                    },
                    phone: {
                        required: "Please enter your phone number"
                    },
                    address: "required",

                },
                submitHandler: function (form) {
                    form.submit();
                }

                // any other options and/or rules
            });
        });
        // jquery.validator.addMethod("BSEmail", function (value, element) {
        //     return this.optional(element) || /^.+@beetsoft.com$/.test(value);
        // }, "Only beet soft.com email address are allowed.");
    </script>
@endsection

