@extends('adminlte::page')
@section('content')


    <body>
    <div class="container mt-5">
        <h2 class="mb-4">All report</h2>
        <div class="col-md-12 text-right mb-5">
            <a class="btn btn-success" href="javascript:void(0)" id="createNewReport"> Create New Report</a>
        </div>
        <table class="table table-bordered tableReport">
            <thead>
            <tr>
                <th>No</th>
                <th>Project ID</th>
                <th>User ID</th>
                <th>Role</th>
                <th>Working hour</th>
                <th>Work Type</th>
                <th>Detail</th>
                <th>Report Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
        <div class="modal fade" id="create-update-report" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>
                    <div class="modal-body">
                        <form id="reportForm" name="reportForm" class="form-horizontal">
                            <input type="hidden" name="id" id="id">
                            <div clas="form-group">
                                <div class="col-sm-12">
                                    <x-adminlte-select id="project_name" name="project_name" label="Project Name" label-class="text-lightblue"
                                                       igroup-size="lg">
                                        <x-slot name="prependSlot">
                                            <div class="input-group-text bg-gradient-info">
                                                <i class="fas fa-user-alt"></i>
                                            </div>
                                        </x-slot>
                                        <option></option>
                                        @foreach($projects_name as $key)
                                        <option>{{$key->project_name}}</option>
                                        @endforeach
                                        <optin>Other</optin>
                                    </x-adminlte-select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <x-adminlte-select name="role" label="Role" label-class="text-lightblue"
                                                       igroup-size="lg">
                                        <x-slot name="prependSlot">
                                            <div class="input-group-text bg-gradient-info">
                                                <i class="fas fa-user-alt"></i>
                                            </div>
                                        </x-slot>
                                        <option></option>
                                        <option>Developer</option>
                                        <option>Tester</option>
                                        <option>Comtor</option>
                                        <option>Brse</option>
                                        <option>PM</option>
                                        <option>Other</option>
                                    </x-adminlte-select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <x-adminlte-select id="working_type" name="working_type" label="Working type" label-class="text-lightblue"
                                                       igroup-size="lg">
                                        <x-slot name="prependSlot">
                                            <div class="input-group-text bg-gradient-info">
                                                <i class="fas fa-user-alt"></i>
                                            </div>
                                        </x-slot>
                                        <option></option>
                                        <option>remote</option>
                                        <option>offsite</option>
                                        <option>onsite</option>
                                        <option>off</option>

                                    </x-adminlte-select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label text-lightblue">Working hours</label>
                                <div class="col-sm-12">
                                    <input type="number" class="form-control" id="working_hour" name="working_hour"
                                           placeholder="Enter the number of working hours" value="" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label text-lightblue">Detail</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control" id="work_detail" name="work_detail" value="" rows="2" placeholder="Job summary here"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" id="saveReport" value="create">Save changes
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </body>

    @endsection
    @section('js')

        <script type="text/javascript">
            $(function () {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var table = $('.tableReport').DataTable({
                    "bDestroy": true,
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('reports.index') }}",
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                        {data: 'project_name', name: 'project_name'},
                        {data: 'user_name', name: 'user_name'},
                        {data: 'role', name: 'role'},
                        {data: 'working_time', name: 'working_time'},
                        {data: 'working_type', name: 'working_type'},
                        {data: 'work_detail', name: 'work_detail'},
                        {data: 'report_date', name: 'report_date'},
                        {
                            data: 'action',
                            name: 'action',
                            orderable: true,
                            searchable: true
                        },
                    ]
                });

            });
            $('#createNewReport').click(function () {
                $('#saveReport').val("create-report");
                $('#id').val('');
                $('#reportForm').trigger("reset");
                $('#modelHeading').html("Create New Report");
                $('#create-update-report').modal('show');
            });

            $('body').on('click', '.editReport', function () {
                var id = $(this).data('id');
                $.get("{{ route('reports.index') }}" + '/' + id + '/edit', function (data) {
                    $('#modelHeading').html("Edit Report");
                    $('#saveReport').val("edit-report");
                    $('#create-update-report').modal('show');
                    $('#id').val(data.id);
                    $('#project_name').val(data[0].project_name);
                    $('#role').val(data[0].role);
                    $('#working_type').val(data[0].working_type);
                    $('#working_hour').val(data[0].working_time);
                    $('#work_detail').val(data[0].work_detail);
                })
            });

            $('#saveReport').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');

                $.ajax({
                    data: $('#reportForm').serialize(),
                    url: "{{ route('reports.store') }}",
                    type: "POST",
                    dataType: 'json',
                    success: function (data) {
                        $('#reportForm').trigger("reset");
                        $('#create-update-report').modal('hide');
                        location.reload();
                        table.draw();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                        $('#saveReport').html('Save Changes');
                    }
                });
            });
            $('body').on('click', '.deleteReport', function () {
                var id = $(this).data("id");
                var result = confirm("Are You sure want to delete Report !");
                if (result) {
                    $.ajax({
                        type: "DELETE",
                        url: "{{ route('reports.store') }}" + '/' + id,
                        success: function (data) {
                            location.reload();
                            table.draw();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                } else {
                    return false;
                }
            });
        </script>
@endsection

