<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    use HasFactory;
    protected $table = 'work_project';
    protected $fillable = [
        'user_id',
        'project_id',
        'role',
        'working_time',
        'working_type',
        'detail',
    ];
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
