<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $table = 'projects';

    protected $fillable = [
        'project_name',
        'detail',
        'duration_from',
        'duration_to',
        'revenue',
    ];
    public function work()
    {
        return $this->hasMany('App\Models\Work');
    }

}
