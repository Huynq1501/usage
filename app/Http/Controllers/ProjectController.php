<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use DataTables;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Project::latest()->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    data-id="' . $row->id . '"
                    data-original-title="Edit"
                    class="edit btn btn-primary btn-sm editProject">
                        Edit
                    </a>';
                    $btn .=' <a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    data-id="' . $row->id . '
                    " data-original-title="Delete"
                    class="btn btn-danger btn-sm deleteProject">
                    Delete
                    </a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.project.list');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Project::updateOrCreate(['id' => $request->id],
            [
                'project_name' => $request->project_name,
                'detail' => $request->detail,
                'password' => $request->password,
                'duration_from' => $request->duration_from,
                'duration_to' => $request->duration_to,
                'revenue' => $request->revenue
            ]
        );

        return response()->json(['success' => 'Project saved successfully.']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);

        return response()->json($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Project::find($id)->delete();

        return response()->json(['success' => 'Project deleted successfully.']);
    }
}
