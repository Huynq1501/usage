<?php

namespace App\Http\Controllers;

use App\Models\Work;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $projects_name= DB::table('projects')->select('project_name')->get();
        if ($request->ajax()) {
            $data = DB::table('work_project')
                ->select(
                    'work_project.id',
                    'role',
                    'user_id',
                    'project_id',
                    'working_time',
                    'working_type',
                    'work_project.detail as work_detail',
                    'project_name',
                    'work_project.created_at as report_date',
                    'users.name as user_name'
                )
                ->join('users', 'work_project.user_id', '=', 'users.id')
                ->join('projects', 'work_project.project_id', '=', 'projects.id')
//                ->where('user_id',Auth::user()->id)
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row)
                {
                    $btn = '<a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    data-id="' . $row->id . '
                    " data-original-title="Edit"
                    class="edit btn btn-primary btn-sm editReport">
                    Edit
                    </a>';
                    $btn .= ' <a
                    href="javascript:void(0)"
                    data-toggle="tooltip"
                    data-id="' . $row->id . '
                    " data-original-title="Delete"
                    class="btn btn-danger btn-sm deleteReport">
                    Delete
                    </a>';

                    return $btn;
                }
                )
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.report.report_user',compact('projects_name'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $time = Carbon::now ('Asia/Ho_Chi_Minh');
        $user_id = Auth::id();
        $project_id = DB::table('projects')->where('project_name',"Project 4")->first()->id;
        Work::updateOrCreate(['id' => $request->id],
            [
                'user_id'=>$user_id,
                'project_id' => $project_id,
                'role' => $request->role,
                'working_time' => $request->working_hour,
                'working_type' => $request->working_type,
                'detail' => $request->work_detail,
                'created_at'=>$time->toDateString(),
                'updated_at'=>$time->toDateString(),
            ]
        );

        return response()->json(['success' => 'Report saved successfully.']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $data = DB::table('work_project')
            ->select(
                'work_project.id as work_id',
                'work_project.id',
                'role',
                'user_id',
                'project_id',
                'working_time',
                'working_type',
                'work_project.detail as work_detail',
                'project_name',
            )
            ->join('users', 'work_project.user_id', '=', 'users.id')
            ->join('projects', 'work_project.project_id', '=', 'projects.id')
            ->where('work_project.id',$id)
            ->get();

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Work::find($id)->delete();

        return response()->json(['success' => 'User deleted successfully.']);
    }
}
