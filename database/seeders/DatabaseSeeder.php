<?php

namespace Database\Seeders;
use App\Models\Project;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $gender = $faker->randomElement(['male', 'female']);

//        foreach (range(1,200) as $index) {
//            DB::table('users')->insert([
//                'name' => $faker->name($gender),
//                'email' => $faker->email,
//                'gender' =>$gender,
//                'password'=>Hash::make(150115),
//                'phone' => $faker->phoneNumber,
//                'birthday' => $faker->date($format = 'Y-m-d', $max = 'now'),
//                 'address' =>Str::random(5),
//            ]);
//        }
        foreach (range(1,10) as $index){
            DB::table('projects')->insert([
                'project_name'=> $faker->text(),
                'detail'=> $faker->text(),
                'duration_from'=>$faker->date(),
                'duration_to'=>$faker->date(),
                'revenue' =>  rand(10000, 99999),
            ]);
        }
    }
}
